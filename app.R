library(shiny)
library(shinydashboard)
library(bigrquery)
library(devtools)
library(dplyr)
library(tidyr)
library(data.table)
library(DT)
library(stringr)
library(highcharter)
library(RColorBrewer)
library(WriteXLS)
library(colourpicker)
library(lubridate)
library(stringr)
library(knitr)
library(kableExtra)

bigrquery::set_service_token('/home/shiny/auth-migo-shiny-bigquery-ro.json')
## global
datasets <- list_datasets(project="datalake-1705")
name <- datasets[datasets %>% str_detect('air_')] %>% gsub("air_","",.) %>% subset(. != "config")

## UI. ##########
header <- dashboardHeader(title = img(src='MIGOxAIR_logo.png', align = "center", height = "40px"))

sidebar <- dashboardSidebar(
  selectInput(inputId = "datasets_name", label = "Client:",
              choices = name, selected = c("nbtw")),
  dateRangeInput(inputId = "setting_report_date_input", label = "Report Date Range:",
                 min = "2016-01-01", max = "2020-12-31",
                 start = "2018-01-01", end = "2018-01-31"),
  dateRangeInput(inputId = "setting_comparison_date_input", label = "Comparison Date Range:",
                 min = "2016-01-01", max = "2020-12-31",
                 start = "2017-12-01", end = "2017-12-31"),
  tags$hr(),
  actionButton("button_submit", strong("Apply")),
  tags$style(type='text/css', "#button_submit { width:80%; margin-left: 20px;}"),
  tags$head(tags$style(HTML(".shiny-notification {position:fixed; top: calc(47%); left: calc(0.4%); width: 16em } ")))
)

body <- dashboardBody(title = "Migo AIR Dashboard",
                      fluidRow(
                        tabBox(
                          side = 'left', width = 12, id = 'tabpnl',
                          tabPanel("Traffic",
                            fluidRow(
                              column(width = 8, style='padding-right:0px;',
                                     dataTableOutput("snapshot_traffic")),
                              column(width = 4, style='padding-left:0px;',
                                     dataTableOutput("snapshot_traffic_txn")))
                          ),
                          tabPanel('Virtual Member',
                            fluidRow(
                              column(width = 8, style='padding-right:0px;',
                                    dataTableOutput("snapshot_vm_re"),
                                    dataTableOutput("snapshot_vm_new")),
                              column(width = 4, style='padding-left:0px;',
                                    dataTableOutput("snapshot_vm_txn"))
                              )
                          )
                        ),
                        uiOutput("box_render"),
                        tags$style(type='text/css', "#snapshot_vm_txn { width:100%; margin-top: 80px;}"),
                        box(width = 12, #height = "3000px",
                            highchartOutput(outputId = "trend_chart_capture_rate"),
                            tags$hr(style="border-top: 1.5px solid #DDDDDD;"),
                            conditionalPanel(
                              condition = "input.tabpnl != 'Traffic'",
                              highchartOutput(outputId = "trend_chart_active_rate"),
                              tags$hr(style="border-top: 1.5px solid #DDDDDD;")
                            ),
                            highchartOutput(outputId = "trend_chart_acquisition_rate"),
                            tags$hr(style="border-top: 1.5px solid #DDDDDD;"),
                            highchartOutput(outputId = "trend_chart_freq_dist_overall"),
                            tags$hr(style="border-top: 1.5px solid #DDDDDD;"),
                            conditionalPanel(
                              condition = "input.tabpnl != 'Traffic'",
                              highchartOutput(outputId = "trend_chart_freq_dist_new_re"),
                              tags$hr(style="border-top: 1.5px solid #DDDDDD;")
                            ),
                            highchartOutput(outputId = "trend_chart_paid_rate"),
                            tags$hr(style="border-top: 1.5px solid #DDDDDD;"),
                            highchartOutput(outputId = "trend_chart_atv")
                        )
                      )
)

server <- function(input, output, session){
  source('scripts/snapshot.R')
  source('scripts/trend_chart.R')
  source('scripts/frequency.R')

  snapshot_dt <- reactiveValues(a = data, b = data)
  trend_chart_dt <- reactiveValues(a = data, b = data)
  frequency_dt <- reactiveValues(a = data, b = data)

  observeEvent(input$button_submit, {
    len1 <- interval(input$setting_report_date_input[1], input$setting_report_date_input[2]) / days(1)
    len2 <- interval(input$setting_comparison_date_input[1], input$setting_comparison_date_input[2]) / days(1)
    if (len1 == len2)
    {
      observeEvent(input$button_submit ,{
        withProgress(message = 'GoGoMigo', min = 0, max = 1, value = 0,
        {
          incProgress(0.1, detail = "Snapshot!")
          snapshot_dt$a <- snapshot(client = input$datasets_name,
                                    start_date = input$setting_report_date_input[1],
                                    end_date = input$setting_report_date_input[2],
                                    time_adjust = 0,
                                    return_script = F)
          incProgress(0.3, detail = "Snapshot2!")
          snapshot_dt$b <- snapshot(client = input$datasets_name,
                                    start_date = input$setting_comparison_date_input[1],
                                    end_date = input$setting_comparison_date_input[2],
                                    time_adjust = 0,
                                    return_script = F)
          incProgress(0.5, detail = "Trend Chart!")
          trend_chart_dt$a <- trend_chart(client = input$datasets_name,
                                          start_date = input$setting_report_date_input[1],
                                          end_date = input$setting_report_date_input[2],
                                          time_adjust = 0,
                                          return_script = F) %>%
                              filter(rpt == "D") %>%
                              mutate(TIME = ymd(TIME)) %>%
                              left_join(worh %>% mutate(date = ymd(date)), by = c("TIME" = "date")) %>%
                              mutate(TIME = format(ymd(TIME), format = "%m/%d"))
          incProgress(0.7, detail = "Trend Chart2!")
          trend_chart_dt$b <- trend_chart(client = input$datasets_name,
                                          start_date = input$setting_comparison_date_input[1],
                                          end_date = input$setting_comparison_date_input[2],
                                          time_adjust = 0,
                                          return_script = F) %>%
                              filter(rpt == "D") %>%
                              mutate(TIME = ymd(TIME)) %>%
                              left_join(worh %>% mutate(date = ymd(date)), by = c("TIME" = "date")) %>%
                              mutate(TIME = format(ymd(TIME), format = "%m/%d"))
          incProgress(0.9, detail = "Frequency!")
          frequency_dt$a <- frequency(client = input$datasets_name,
                                      start_date = input$setting_report_date_input[1],
                                      end_date = input$setting_report_date_input[2],
                                      time_adjust = 0,
                                      return_script = F)
          incProgress(0.99, detail = "Frequency2!")
          frequency_dt$b <- frequency(client = input$datasets_name,
                                      start_date = input$setting_comparison_date_input[1],
                                      end_date = input$setting_comparison_date_input[2],
                                      time_adjust = 0,
                                      return_script = F)
        })
      })

      worh <- fread("worh.csv")
      eventReactive(input$button_submit,
      {
        trend_chart_dt$a$TIME[which(trend_chart_dt$a$tw_worh == "Holiday")]
      }) -> date

      observeEvent(input$button_submit, {
        # Snapshot ####
        output$snapshot_title <- renderText({
          paste("REVENUE  ", (snapshot_dt$a$n_TXN * snapshot_dt$a$ATV) %>% format(.,big.mark=",",scientific=FALSE))
        })

        output$snapshot_traffic <- renderDataTable({
          snapshot_dt$a %>%
            mutate_at(vars(Capture_Rate), funs(paste0(round(.*100, 2), "%"))) %>%
            mutate_at(vars(Frequency), funs(round(., 2))) %>%
            mutate_at(vars(Surrounding, Walkin), funs(format(.,big.mark=",",scientific=FALSE))) %>%
            mutate_all(funs(as.character(.))) %>%
            bind_rows(
              snapshot_dt$a %>%
                bind_rows(snapshot_dt$b) %>%
                mutate_all(funs(paste0(round((. - lead(.))*100/lead(.), 2), "%"))) %>%
                slice(1)
            ) %>%
            select(Surrounding = Surrounding, `Capture Rate` = Capture_Rate, `Walk-ins` = Walkin, Frequency = Frequency) %>%
            datatable(options = list(dom = 't', ordering = FALSE,
                                     columnDefs = list(list(className = 'dt-center', targets = 0:3),
                                                       list(width = '25%', targets = 0:3))),
                      style = 'bootstrap', class = 'table-hover', rownames = F) %>%
            #formatCurrency(c("Surrounding", "Walk-ins"), currency = "", interval = 3, mark = ",", digits = 0) %>%
            formatStyle(c("Capture Rate", "Frequency"), backgroundColor = "#D5E7F2", fontWeight = 'bold')
        })

        output$snapshot_traffic_txn <- renderDataTable({
          snapshot_dt$a %>%
            mutate_at(vars(ATV), funs(round(., 2))) %>%
            mutate_at(vars(Paid_Rate), funs(paste0(round(.*100, 2), "%"))) %>%
            mutate_at(vars(n_TXN, ATV), funs(format(.,big.mark=",",scientific=FALSE))) %>%
            mutate_all(funs(as.character(.))) %>%
            bind_rows(
              snapshot_dt$a %>%
                bind_rows(snapshot_dt$b) %>%
                mutate_all(funs(paste0(round((. - lead(.))*100/lead(.), 2), "%"))) %>%
                slice(1)
            ) %>%
            select(`Paid Rate` = Paid_Rate, `# of TXN` = n_TXN, ATV = ATV) %>%
            datatable(options = list(dom = 't', ordering = FALSE,
                                     columnDefs = list(list(className = 'dt-center', targets = 0:2))),
                      style = 'bootstrap', class = 'table-hover', rownames = F) %>%
            # #formatCurrency(c("# of TXN", "ATV"), currency = "", interval = 3, mark = ",", digits = 0) %>%
            formatStyle(c("Paid Rate", "ATV"), backgroundColor = "#D5E7F2", fontWeight = 'bold')
        })

        output$snapshot_vm_re <- renderDataTable({
          snapshot_dt$a %>%
            mutate_at(vars(Active_Rate), funs(paste0(round(.*100, 2), "%"))) %>%
            mutate_at(vars(Revisitor_Frequency), funs(round(., 2))) %>%
            mutate_at(vars(Virtual_Member, Revisitor), funs(format(.,big.mark=",",scientific=FALSE))) %>%
            mutate_all(funs(as.character(.))) %>%
            bind_rows(
              snapshot_dt$a %>%
                bind_rows(snapshot_dt$b) %>%
                mutate_all(funs(paste0(round((. - lead(.))*100/lead(.), 2), "%"))) %>%
                slice(1)
            ) %>%
            select(`Virtual Member` = Virtual_Member, `Active Rate`  = Active_Rate,
                   `Re-visitor` = Revisitor, `Re-visitor Frequency` = Revisitor_Frequency) %>%
            datatable(options = list(dom = 't', ordering = FALSE,
                                     columnDefs = list(list(className = 'dt-center', targets = 0:3),
                                                       list(width = '25%', targets = 0:3))),
                      style = 'bootstrap', class = 'table-hover', rownames = F) %>%
            #formatCurrency(c("Virtual Member", "Re-visitor"), currency = "", interval = 3, mark = ",", digits = 0) %>%
            formatStyle(c("Active Rate", "Re-visitor Frequency"), backgroundColor = "#A1BAA9", fontWeight = 'bold')
        })

        output$snapshot_vm_new <- renderDataTable({
          snapshot_dt$a %>%
            mutate_at(vars(New_Capture_Rate), funs(paste0(round(.*100, 2), "%"))) %>%
            mutate_at(vars(Newvisitor_Frequency), funs(round(., 2))) %>%
            mutate_at(vars(Potential_Surrounding, Newvisitor), funs(format(.,big.mark=",",scientific=FALSE))) %>%
            mutate_all(funs(as.character(.))) %>%
            bind_rows(
              snapshot_dt$a %>%
                bind_rows(snapshot_dt$b) %>%
                mutate_all(funs(paste0(round((. - lead(.))*100/lead(.), 2), "%"))) %>%
                slice(1)
            ) %>%
            select(`New Potential Surrounding` = Potential_Surrounding, `Acquisition Rate` = New_Capture_Rate,
                   `New-visitor` = Newvisitor, `New-visitor Frequency` = Newvisitor_Frequency) %>%
            datatable(options = list(dom = 't', ordering = FALSE,
                                     columnDefs = list(list(className = 'dt-center', targets = 0:3),
                                                       list(width = '25%', targets = 0:3))),
                      style = 'bootstrap', class = 'table-hover', rownames = F) %>%
            #formatCurrency(c("New Potential Surrounding", "New-visitor"), currency = "", interval = 3, mark = ",", digits = 0) %>%
            formatStyle(c("Acquisition Rate", "New-visitor Frequency"), backgroundColor = "#C9F3C9", fontWeight = 'bold')
        })

        output$snapshot_vm_txn <- renderDataTable({
          snapshot_dt$a %>%
            mutate_at(vars(ATV), funs(round(., 2))) %>%
            mutate_at(vars(Paid_Rate), funs(paste0(round(.*100, 2), "%"))) %>%
            mutate_at(vars(n_TXN, ATV), funs(format(.,big.mark=",",scientific=FALSE))) %>%
            mutate_all(funs(as.character(.))) %>%
            bind_rows(
              snapshot_dt$a %>%
                bind_rows(snapshot_dt$b) %>%
                mutate_all(funs(paste0(round((. - lead(.))*100/lead(.), 2), "%"))) %>%
                slice(1)
            ) %>%
            select(`Paid Rate` = Paid_Rate, `# of TXN` = n_TXN, ATV = ATV) %>%
            datatable(options = list(dom = 't', ordering = FALSE,
                                     columnDefs = list(list(className = 'dt-center', targets = 0:2))),
                      style = 'bootstrap', class = 'table-hover', rownames = F) %>%
            #formatCurrency(c("# of TXN", "ATV"), currency = "", interval = 3, mark = ",", digits = 0) %>%
            formatStyle(c("Paid Rate", "ATV"), backgroundColor = "#D5E7F2", fontWeight = 'bold')
        })

        output$box_render <- renderUI({
          box(width = 12, background = "aqua", align = "center",
              div(style = "font-size:20px;", strong(textOutput("snapshot_title"))))
        })

      # Trend Chart ####
        source("plot_trend_chart.R")
        output$trend_chart_capture_rate <- renderHighchart({
          trend_chart_dt$a %>%
            select(TIME, Surrounding, Capture_Rate) %>%
            mutate(Capture_Rate = Capture_Rate*100) -> plot_df

          trend_chart_dt$b %>%
            select(TIME, Surrounding, Capture_Rate) %>%
            mutate(Capture_Rate = Capture_Rate*100) -> plot_df2

          plot_daily_capture_rate(plot_df, date(), plot_df2)
        })

        output$trend_chart_active_rate <- renderHighchart({
          if(input$tabpnl != 'Traffic'){
                    trend_chart_dt$a %>%
                      select(TIME, n_vm, Active_Rate) %>%
                      mutate(Active_Rate = Active_Rate*100) -> plot_df

                    trend_chart_dt$b %>%
                      select(TIME, n_vm, Active_Rate) %>%
                      mutate(Active_Rate = Active_Rate*100) -> plot_df2

                    plot_daily_active_rate(plot_df, date(), plot_df2)
          }
        })

        output$trend_chart_acquisition_rate <- renderHighchart({
          trend_chart_dt$a %>%
            select(TIME, Potential_Surrounding, New_Capture_Rate) %>%
            mutate(New_Capture_Rate = New_Capture_Rate*100) -> plot_df

          trend_chart_dt$b %>%
            select(TIME, Potential_Surrounding, New_Capture_Rate) %>%
            mutate(New_Capture_Rate = New_Capture_Rate*100) -> plot_df2

          plot_daily_acquisition_rate(plot_df, date(), plot_df2)
        })

        output$trend_chart_freq_dist_overall <- renderHighchart({
          frequency_dt$a %>%
            filter(Frequency >= 2) %>%
            mutate(Frequency = cut(Frequency,
                                   c(2,3,4,5,6,7,8,9,10,Inf),
                                   c("2","3","4","5","6","7","8","9","10+"),
                                   include.lowest = TRUE, right = FALSE)) %>%
            group_by(Frequency) %>%
            summarise(n = sum(n)) %>%
            mutate(Type = "Walk-ins") -> plot_df

          frequency_dt$b %>%
            filter(Frequency >= 2) %>%
            mutate(Frequency = cut(Frequency,
                                   c(2,3,4,5,6,7,8,9,10,Inf),
                                   c("2","3","4","5","6","7","8","9","10+"),
                                   include.lowest = TRUE, right = FALSE)) %>%
            group_by(Frequency) %>%
            summarise(n = sum(n)) %>%
            mutate(Type = "Walk-ins") -> plot_df2

          plot_daily_freq_overall(plot_df, date(), plot_df2)
        })

        output$trend_chart_freq_dist_new_re <- renderHighchart({
          frequency_dt$a %>%
            filter(Frequency >= 2) %>%
            mutate(Frequency = cut(Frequency,
                                   c(2,3,4,5,6,7,8,9,10,Inf),
                                   c("2","3","4","5","6","7","8","9","10+"),
                                   include.lowest = TRUE, right = FALSE),
                   Type = plyr::mapvalues(Type,
                                          c("New","Old"),
                                          c("New-visitor","Re-visitor"))) %>%
            group_by(Type, Frequency) %>%
            summarise(n = sum(n)) -> plot_df

          frequency_dt$b %>%
            filter(Frequency >= 2) %>%
            mutate(Frequency = cut(Frequency,
                                   c(2,3,4,5,6,7,8,9,10,Inf),
                                   c("2","3","4","5","6","7","8","9","10+"),
                                   include.lowest = TRUE, right = FALSE),
                   Type = plyr::mapvalues(Type,
                                          c("New","Old"),
                                          c("New-visitor","Re-visitor"))) %>%
            group_by(Type, Frequency) %>%
            summarise(n = sum(n)) -> plot_df2

          plot_daily_freq_new_re(plot_df, date(), plot_df2)

        })

        output$trend_chart_paid_rate <- renderHighchart({
          trend_chart_dt$a %>%
            select(TIME, n_Visitor, Paid_Rate) %>%
            mutate(Paid_Rate = Paid_Rate*100) -> plot_df

          trend_chart_dt$b %>%
            select(TIME, n_Visitor, Paid_Rate) %>%
            mutate(Paid_Rate = Paid_Rate*100) -> plot_df2

          plot_daily_paid_rate(plot_df, date(), plot_df2)
        })

        output$trend_chart_atv <- renderHighchart({
          trend_chart_dt$a %>%
            select(TIME, n_TXN, ATV) -> plot_df

          trend_chart_dt$b %>%
            select(TIME, n_TXN, ATV) -> plot_df2

          plot_daily_atv(plot_df, date(), plot_df2)
        })
      })
    }
    else
      showNotification("對比需要相同的時間長度唷！", type = "error")
  })
}

shinyApp(ui = dashboardPage(title = "AIR Dashboard", header, sidebar, body, skin = "black"), server)
